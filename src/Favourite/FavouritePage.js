import React,{useEffect,useState,useContext} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    FlatList,
} from 'react-native';
import { Context as BlogContext } from '../BlogContext';
import { TouchableHighlight } from 'react-native-gesture-handler';
import CardHome from '../Home/CardHome';
// import API from '../API'
// import { TouchableHighlight } from 'react-native-gesture-handler';



const FavouritePage = (props) => {
    const { state } = useContext(BlogContext)
    const { navigation } = props;
    return (
        <View>
            <FlatList
            data={state}
            keyExtractor={item => item.id.toString()}
            renderItem={({ item }) => {
                return (
                    <TouchableHighlight onPress={() => navigation.navigate('DetailPage',{urlImg:item.urlImg,nameMovie:item.nameMovie,date:item.date,detail:item.detail,vote:item.vote,id:item.id})} >
                        <View>
                            
                            <CardHome urlImg={`https://image.tmdb.org/t/p/w92${item.urlImg}`} nameMovie={item.nameMovie} date={item.date} detail={item.detail}/>
                        </View>
                    </TouchableHighlight>
                )
            }}
        />
        </View>
  );
};

const styles = StyleSheet.create({

            });

export default FavouritePage;
