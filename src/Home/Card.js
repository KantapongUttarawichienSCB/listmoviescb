import React from 'react';
import { Text, View, Image, StyleSheet, TouchableOpacity, Button } from 'react-native';
import Component from 'react';


const styles = StyleSheet.create({
    containerBox: {
        flexDirection: 'column',
        marginLeft: 20,
        marginTop: 30,
        // justifyContent: 'space-between',
        height: 90,
    },
    imgSet: {
        width: 100,
        height: 100,
        marginLeft: 20,
        marginTop: 20,
        borderRadius: 10,

    },
    containerText: {
        flexDirection: 'row',
        // backgroundColor: 'red',
        width:240,
       
    },
    textBold: {
        marginLeft:20,
        fontSize: 20,
        alignSelf: 'center',
    }

});


const Card = ({ nameMovie }) => {
    return (
        <View style={{ flexDirection: 'row', backgroundColor: 'mintcream', height: 40, marginTop: 20, marginLeft: 20, borderRadius: 10, marginRight: 20,alignItems:'center', }}>
            <Text style={styles.textBold}>{nameMovie}</Text>
        </View>
    )

};
Card.defaultProps = {
    nameMovie: 'noName',
};


export default Card;