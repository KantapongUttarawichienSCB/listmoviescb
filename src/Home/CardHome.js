import React from 'react';
import { Text, View, Image, StyleSheet, TouchableOpacity, Button } from 'react-native';
import Component from 'react';


const styles = StyleSheet.create({
    containerBox: {
        flexDirection: 'column',
        marginLeft: 20,
        marginTop: 30,
        // justifyContent: 'space-between',
        height: 90,
    },
    imgSet: {
        width: 100,
        height: 100,
        marginLeft: 20,
        marginTop: 20,
        borderRadius: 10,

    },
    containerText: {
        flexDirection: 'row',
        // backgroundColor: 'red',
        width:240,
       
    },
    textBold: {
        fontWeight: 'bold',
        marginRight: 5,
    }

});


const CardHome = ({ urlImg, nameMovie, date, detail, }) => {

    var check = true
    //const navigation = useNavigation();



    return (
        <View style={{ flexDirection: 'row', backgroundColor: 'mintcream', height: 140, marginTop: 10, marginLeft: 10, borderRadius: 10, marginRight: 10, }}>
            {/* <Text>Hello SCB</Text>, */}
            {/* <TouchableOpacity onPress={onSent}><Image style={styles.imgSet}source={{ uri: urlImg,}} /></TouchableOpacity> */}
            <Image style={styles.imgSet} source={{ uri: urlImg, }} />
            <View style={styles.containerBox}>
                <View style={styles.containerText}>
                    <Text style={styles.textBold}>{nameMovie}</Text>
                    
                </View>
                <View style={styles.containerText}>
                    <Text>{date}</Text>
                </View>
                <View style={styles.containerText}>
                    <Text style={{marginBottom: 30,}}>{detail}</Text>
                </View>
                {/* <View style={styles.containerText}>
                    <Text style={styles.textBold}>Price : </Text>
                    <Text>{price}</Text>
                </View> */}
                {/* <View style={styles.containerText}>
                    <Text style={styles.textBold}>Like : </Text>
                    <Text>{count}</Text>
                </View> */}
                {/* <Button title="Go to Details" onPress={() => {
                    navigation.navigate('Detail', {
                        urlImg, nameImg, url, ownerName,
                    })
                }} /> */}

            </View>

        </View>
    )

};
CardHome.defaultProps = {
    urlImg: 'https://images.pexels.com/photos/995022/pexels-photo-995022.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',
    nameMovie: 'noName',
    date: 'noName',
    detail: '3asd,asopfkdoskvokaspd,qwroewrhimr3asd,asopfkdoskvokaspd,qwroewrhimr3asd,asopfkdoskvok3asd,asopfkdoskvokaspd,qwroewrhimr3asd,asopfkdoskvokaspd,qwroewrhimr3asd,asopfkdoskvokaspd,qwroewrhimraspd,qwroewrhimr',
};

export default CardHome;