import React, { Component, useState, useEffect, useContext } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    Button,
    TextInput,
    FlatList,

} from 'react-native';
import Card from './Card'
import { TouchableHighlight } from 'react-native-gesture-handler';
import { Context as TitleHistory } from '../TitleHistory';


const HomePage = (props) => {
    const [value, SearchText] = React.useState('');
    const { navigation } = props;
    const { state, addBlog } = useContext(TitleHistory)

    navigation.setOptions({
        headerRight: () => (
            <Button
                // onPress={() => {
                // }}
                onPress={() => navigation.navigate('FavouritePage')}

                title="Favorite"
                color='black'
            />
        ),
    })


    return (
        <View style={{ flexDirection: 'column', }}>
            <View style={{ marginTop: 0, marginLeft: 20, flexDirection: 'row', }}>
                <TextInput
                    style={{ marginTop: 20, height: 40, borderColor: 'gray', borderWidth: 1, borderRadius: 20, width: 250, }}
                    onChangeText={text => SearchText(text)}
                    placeholder='Lastname'
                    value={value}
                />
                <View style={{ marginTop: 20, marginLeft: 10, marginRight: 20, backgroundColor: 'gainsboro', borderRadius: 20, width: 110, }}>
                    <Button title="Search" onPress={() => {
                        navigation.navigate('SearchPage', { textSearch: value }) & addBlog(value)
                        // for (var i = 0; i <= state.length; i++) {
                        //     console.log(nameMovie,value);
                        //     if (state.nameMovie == value.nameMovie && state.length != 0) {
                        //         console.log(66666);
                        //         navigation.navigate('SearchPage', { textSearch: value })
                        //         break;
                        //     } else if (state.nameMovie != value){
                        //         console.log(5555);
                                
                        //         navigation.navigate('SearchPage', { textSearch: value }) & addBlog(value)
                        //         break;
                        //     }
                        // }
                        
                    }}
                    />
                </View>
            </View>
            <FlatList
                data={state}
                keyExtractor={item => item.id.toString()}
                renderItem={({ item }) => {
                    return (
                        <TouchableHighlight onPress={() => navigation.navigate('SearchPage', { textSearch: item.nameMovie })} >
                            <View>
                                < Card nameMovie={item.nameMovie} />
                            </View>
                        </TouchableHighlight>
                    )
                }}
            />
        </View>
    );
};

const styles = StyleSheet.create({

});

export default HomePage;
