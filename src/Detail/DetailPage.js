import React, { Component,useContext } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  Image,
  Button,
} from 'react-native';
import { Context as BlogContext } from '../BlogContext';

const DetailPage = (props) => {
  const { navigation } = props;
  navigation.setOptions({
    headerRight: () => (
        <Button
            // onPress={() => {
            // }}
            onPress={() => navigation.navigate('HomePage')}

            title="HomePage"
            color='black'
        />
    ),
})
  
  const { state,addBlog,deleteBlog } = useContext(BlogContext)
  const { nameMovie,vote,detail,urlImg,date,id } = props.route.params;

  var check = false

  for (var i = 0;i < state.length;i++){
    if (state[i].id == id){
      check = true;
      break;
    }else {
      check = false
    }
  }

  return (
    <View>
      <View style={{ flexDirection: 'column', backgroundColor: 'mintcream', marginTop: 10, marginLeft: 10, borderRadius: 10, marginRight: 10, }}>
        <Image style={styles.imgSet} source={{ uri: `https://image.tmdb.org/t/p/w92${urlImg}` }} />
        <Text style={styles.textBold}>{nameMovie}</Text>
        <Text style={styles.textNormal}> Average votes : {vote}</Text>
        <Text style={styles.textNormal}>{detail}</Text>
      </View>
      <View style={{ flexDirection: 'column', backgroundColor: 'lightskyblue', marginTop: 20, marginLeft: 20, borderRadius: 10, marginRight: 20,height:40, }}>
      <Button title={ check ? "Unfavourite" : "favourite"} onPress={() => check ? deleteBlog(id) : addBlog(nameMovie,vote,detail,urlImg,date,id)} />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  imgSet: {
    width: 200,
    height: 300,
    marginTop: 20,
    borderRadius: 10,
    alignSelf: 'center',
  },
  textNormal: {
    marginRight: 30,
    marginLeft: 30,
    fontSize: 16,
    marginTop: 5,
    marginBottom: 15,
  },
  textBold: {
    fontWeight: 'bold',
    marginLeft: 30,
    fontSize: 20,
    marginTop: 30,
    marginBottom: 10,
  }

});

export default DetailPage;
