import React,{useEffect,useState} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
    FlatList,
} from 'react-native';
import API from '../API'
import { TouchableHighlight } from 'react-native-gesture-handler';
import CardHome from '../Home/CardHome';


const SearchPage = (props) => {
    const { textSearch } = props.route.params;
    const { navigation } = props;
    const [dataMobile, setDataMobile] = useState([])

    

    useEffect(() => {

        fetchMovie()
        //console.log(55);
       
    
    },[]);
    
    const fetchMovie = async () => {
        const response = await API.get(`/api/movies/search?query=${textSearch}&page=1`);
        //dataMobile = response
        //console.log(response.data.results);
        setDataMobile(response.data.results)
    
    };

    return (
        <View>
            <FlatList
            data={dataMobile}
            keyExtractor={item => item.id.toString()}
            renderItem={({ item }) => {
                return (
                    <TouchableHighlight onPress={() => navigation.navigate('DetailPage',{urlImg: item.poster_path,nameMovie:item.original_title,vote:item.vote_average,detail:item.overview,date:item.release_date,id:item.id})}>
                        <View>
                            
                            <CardHome urlImg={`https://image.tmdb.org/t/p/w92${item.poster_path}`} nameMovie={item.original_title} date={item.release_date} detail={item.overview}/>
                        </View>
                    </TouchableHighlight>
                )
            }}
        />
        </View>
  );
};

const styles = StyleSheet.create({

            });

export default SearchPage;
