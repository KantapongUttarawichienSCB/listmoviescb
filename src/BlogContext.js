
import createDataContext from './createContext'

const initalState = [
    // { title: 'Blog Post #1',id:1 },
    // { title: 'Blog Post #2',id:2 },
    // { title: 'Blog Post #3',id:3 },
]

const blogReducer = (state, { type, payload }) => {
    switch (type) {
        case 'add_blog':
            return [...state, { nameMovie: payload.nameMovie, vote: payload.vote, detail: payload.detail, urlImg: payload.urlImg, date: payload.date,id: payload.id }];
        case 'delete_blog':
            return state.filter((initalState) => initalState.id !== payload)
        // case 'edit_blog':
        //     return [...state,{title: payload.newname, lasttitle: payload.newlastName,id: newid}]
        default:
            return state;
    }
};

const addBlog = dispatch => {
    return (nameMovie,vote,detail,urlImg,date,id) => {
        dispatch({ type: 'add_blog', payload: { nameMovie,vote,detail,urlImg,date,id } });
    };
};

const deleteBlog = dispatch => {
    return (id) => {
        dispatch({ type: 'delete_blog', payload: id });
    };
};
// const editBlog = dispatch => {
//     return (newname, newlastName, newid) => {
//         dispatch({ type: 'edit_blog', payload: { name, lastName, id } });
//     };
// };

export const { Context, Provider } = createDataContext(
    blogReducer,
    { addBlog ,deleteBlog},
    initalState,
)
