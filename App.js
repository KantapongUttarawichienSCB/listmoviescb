/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import HomePage from './src/Home/HomePage'
import SearchPage from './src/Search/SearchPage'
import DetailPage from './src/Detail/DetailPage'
import FavouritePage from './src/Favourite/FavouritePage'
import { Provider as BlogContextProvider } from './src/BlogContext'
import { Provider as BlogContextHistory } from './src/TitleHistory'


const Stack = createStackNavigator();

const App = () => {
  return (
    <BlogContextProvider>
      <BlogContextHistory>
        <NavigationContainer>
          <Stack.Navigator initialRouteName="HomePage">
            <Stack.Screen name="HomePage" component={HomePage} />
            <Stack.Screen name="SearchPage" component={SearchPage} />
            <Stack.Screen name="DetailPage" component={DetailPage} />
            <Stack.Screen name="FavouritePage" component={FavouritePage} />
          </Stack.Navigator>
        </NavigationContainer>
      </BlogContextHistory>
    </BlogContextProvider>
  );
};

const styles = StyleSheet.create({

});

export default App;
